<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Update List: ${toUpdate.title}</title>
</head>
<body>

<form action="" method="post">

	<fieldset>
		<legend>Update list: ${toUpdate.title}</legend>
		New title: <input type="text" name="title" value="${toUpdate.title}" placeholder="Some title" />
		<br/>
		<input type="submit" value="Update" />
		
	</fieldset>

</form>

</body>
</html>