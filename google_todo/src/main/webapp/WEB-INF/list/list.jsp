<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
 <title>List ToDo</title>
</head>

<table border="1">
		<caption>ToDos</caption>
		<tr>
			<th>Title</th>
		</tr>
		<c:forEach var="List" items="${lists}">
			<tr>
				<td>${List.title}</td>
				<c:forEach var="operation" items="${operations.entrySet()}">
					<td><a href="${pageContext.request.contextPath}/list/${operation.key}?id=${List.id}">${operation.value}</a></td>
				</c:forEach>
			</tr>
		</c:forEach>
		
</table>


<form action="${pageContext.request.contextPath}/list/create" method="post">
	<fieldset>
		<legend>Create List</legend>
			Title List: <input type="text" name="title" placeholder="Some title">
			<br/>
			<input type="submit" value="Create" />
		
	</fieldset>

</form>




</body>
</html>