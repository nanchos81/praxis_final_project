package mx.com.praxis.utils;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import mx.com.praxis.dtos.ListDetailDto;
import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.dtos.TaskDto;
import mx.com.praxis.entities.Task;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Converter {

	
	public static List<ListDto> convertListsToListDto(List<mx.com.praxis.entities.List> source){
		return source
			.stream()
			.map(Converter::convertListToListDto)
			.collect(Collectors.toList());
	}
	
	public static ListDto convertListToListDto(mx.com.praxis.entities.List source) {
		return ListDto.builder()
				.id(source.getId())
				.title(source.getTitle())
				.build();
	}
	
	public static mx.com.praxis.entities.List converterListDtoToList(ListDto source){
		return mx.com.praxis.entities.List.builder()
			.title(source.getTitle())
			.build();
	}
	
	
	public static ListDto converterRequestToListDto(HttpServletRequest request) {
		return ListDto.builder()
			.title(request.getParameter("title"))
			.build();
	}
	
	public static ListDto convertRequestToListDtoUpdate(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("id"));
		String title = request.getParameter("title");
		
		return ListDto.builder()
				.id(id)
				.title(title)
				.build();
	}
	
	public static mx.com.praxis.entities.List convertListDtoToListUpdate(ListDto source){
		return mx.com.praxis.entities.List.builder()
				.id(source.getId())
				.title(source.getTitle())
				.build();
	}
	
	public static List<TaskDto> convertTasksToTaskDetailDtos(List<Task> source){
		return source.stream()
				.map(Converter::convertTaskToTaskDto)
				.collect(Collectors.toList());
	}
	
	public static TaskDto convertTaskToTaskDto(Task source) {
		return TaskDto.builder()
				.id(source.getId())
				.title(source.getTitle())
				.build();
	}
	
	public static ListDetailDto convertListToListDetailDto(mx.com.praxis.entities.List source, List<Task> complement) {
		return ListDetailDto.builder()
				.id(source.getId())
				.title(source.getTitle())
				.tasks(convertTasksToTaskDetailDtos(complement))
				.build();
	}
	
}
