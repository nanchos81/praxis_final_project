package mx.com.praxis.daos;

import java.util.List;

import javax.ejb.Local;
import mx.com.praxis.entities.Task;

@Local
public interface TaskDao extends GenericDao<Task, Long>{
	List<Task> findTaskByIdList(Long id);
	
	List<Long> findIdsByIdList(Long idList);
	
	void deleteByIdList(Long idList);
}
