package mx.com.praxis.daos;

import java.util.List;
import javax.ejb.Local;
import mx.com.praxis.entities.Note;


@Local
public interface NoteDao extends GenericDao<Note, Long>{
	List<Note> findNoteByIdTask(Long id);
	
	void deleteByIdTask(Long idTask);
}
