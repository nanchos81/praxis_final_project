package mx.com.praxis.daos.impl;

import mx.com.praxis.entities.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import mx.com.praxis.daos.ListDao;

@Stateless
public class ListDaoImpl implements ListDao{

	@PersistenceContext(name = "AccessDataBase")
	private  EntityManager entityManager;
	
	@Override
	public java.util.List<List> findAll() {
		TypedQuery<List> queryToFindAllList = entityManager.createNamedQuery("List.findAll", List.class);
		/*Query foo = entityManager.createNamedQuery("List.findAll");
		java.util.List<List> bar = foo.getResultList();*/
		return queryToFindAllList.getResultList();
	}

	@Override
	public List findById(Long id) {
		TypedQuery<List> queryToFindById = entityManager.createNamedQuery("List.findById", List.class);
		queryToFindById.setParameter("id", id);
		return queryToFindById.getSingleResult();
	}

	@Override
	public void create(List toSave) {
		entityManager.persist(toSave);
		
	}

	@Override
	public void update(List toUpdate) {
		entityManager.merge(toUpdate);		
	}

	@Override
	public void delete(List toDelete) {
		entityManager.remove(toDelete);
		
	}
	
	/*public void delete(Long id) {
		delete(findById(id));
	}*/


}
