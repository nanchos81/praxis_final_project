package mx.com.praxis.daos;

import java.util.List;

public interface GenericDao<T, ID> {
	List<T> findAll();

	T findById(ID id);
	void create (T toSave);
	void update (T toUpdate);
	void delete (T toDelete);
}
