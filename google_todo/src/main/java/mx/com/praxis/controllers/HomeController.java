package mx.com.praxis.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.services.ListService;

@WebServlet(urlPatterns = {
		"/"
})
public class HomeController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1990060213398492502L;
	
	private static final Map<String, String> OP = new HashMap<>(3);
	
	static {
		OP.put("read", "Read");
		OP.put("update", "Update");
		OP.put("delete", "Delete");
	}
	
	@Inject
	private ListService listService;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("lists", listService.getAllListDto());
		request.setAttribute("operations", OP);
		request.getRequestDispatcher("/WEB-INF/list/list.jsp").forward(request, response);
	}
	
}
