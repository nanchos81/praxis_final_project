package mx.com.praxis.services;

import java.util.List;

import javax.ejb.Local;

import mx.com.praxis.dtos.ListDetailDto;
import mx.com.praxis.dtos.ListDto;

@Local
public interface ListService {
	List<ListDto> getAllListDto();
	
	void create(ListDto toCreate);
	
	ListDto getListDtoById(Long id);
	
	void update(ListDto toUpdate);
	
	void delete(Long id);
	
	ListDetailDto getListDetailDtoById(Long id);
	
}
