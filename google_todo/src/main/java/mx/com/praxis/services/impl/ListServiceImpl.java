package mx.com.praxis.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import mx.com.praxis.daos.ListDao;
import mx.com.praxis.daos.NoteDao;
import mx.com.praxis.daos.TaskDao;
import mx.com.praxis.dtos.ListDetailDto;
import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.services.ListService;
import mx.com.praxis.utils.Converter;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class ListServiceImpl implements ListService{
	
	@Inject
	private ListDao listDao;
	
	@Inject
	private TaskDao taskDao;	
	
	@Inject
	private NoteDao noteDao;	
	
	@Override
	public List<ListDto> getAllListDto() {
		return Converter.convertListsToListDto(listDao.findAll());

	}

	@Override
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void create(ListDto toCreate) {
		
		listDao.create(Converter.converterListDtoToList(toCreate));

		
	}

	@Override
	public ListDto getListDtoById(Long id) {
		
		return Converter.convertListToListDto(listDao.findById(id));
	}

	@Override
	@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
	public void update(ListDto toUpdate) {
		listDao.update(Converter.convertListDtoToListUpdate(toUpdate));
	}

	@Override
	public void delete(Long id) {
		
		/*taskDao.findTaskByIdList(id)
			.forEach(t -> {
				noteDao.findNoteByIdTask(t.getId())
					.forEach(noteDao::delete);
				taskDao.delete(t);
			});*/
		
		/*taskDao.findTaskByIdList(id)
			.forEach(t -> {noteDao.deleteByIdTask(t.getId());
			taskDao.delete(t);
			});*/
		
		/*taskDao.findIdsByIdList(id)
	
			.forEach(t -> noteDao.deleteByIdTask(t));*/
	
		taskDao.findIdsByIdList(id).forEach(noteDao::deleteByIdTask);
		taskDao.deleteByIdList(id);
		listDao.delete(listDao.findById(id));
	}

	@Override
	public ListDetailDto getListDetailDtoById(Long id) {
		return Converter.convertListToListDetailDto(listDao.findById(id), taskDao.findTaskByIdList(id));
	}

}
