package mx.com.praxis.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "notes")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor (access= AccessLevel.PRIVATE)
@Builder
@NamedQueries({
	@NamedQuery(name = "Note.findAllNoteByIdTask", query = "select n from Note n where n.idTask = : idTask"),
	@NamedQuery(name = "Note.deleteByIdTask", query = "delete from Note n where n.idTask = : idTask")
})
public class Note implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4019024396729413113L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="id_tasks")
	private Long idTask;
	
	@Column(length = 200)
	private String description;
	
	
}
