package mx.com.praxis.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tasks")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor (access= AccessLevel.PRIVATE)
@Builder
@NamedQueries({
	@NamedQuery(name = "Task.findAllTaskByIdList", query = "select t from Task t where t.idList = : idList"),
	@NamedQuery(name = "Task.findIdsByIdList", query = "select t.id from Task t where t.idList = : idList"),
	@NamedQuery(name = "Task.deleteByIdList", query = "delete from Task t where t.idList = : idList")
})
public class Task implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4676254911087754773L;

	/**
	 * 
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="id_lists")
	private Long idList;
	
	@Column(name="is_complete")
	private Boolean isCompleted;
	
	@Column(length = 100)
	private String title;
	
	
}
