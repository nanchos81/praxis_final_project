package mx.com.praxis.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "lists")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor (access= AccessLevel.PRIVATE)
@Builder
@NamedQueries({
	@NamedQuery(name = "List.findAll", query = "select l from List l"),
	@NamedQuery(name = "List.findById", query = "select l from List l where l.id = :id")
})
public class List implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2917821276898136398L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//@Max(value = 100L)
	@Column(length = 100)
	private String title;
	
	
}
